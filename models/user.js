// user.js
import mongoose from "mongoose";
import ImageModel from "./image.js";
import PaymentModel from "./payment.js";
import AddressModel from "./address.js";
import PhoneModel from "./phone.js";

const UserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    phone: [PhoneModel.schema],
    image: [ImageModel.schema],
    properties: [{ type: mongoose.Schema.Types.ObjectId, ref: "Property" }],
    payment: [PaymentModel.schema],
    address: AddressModel.schema,
    role: { type: String, default: "user" },
    status: { type: String, default: "active" },
    createdAt: { type: Date, default: Date.now },
    // Add any other fields as needed
});

const userModel = mongoose.model("User", UserSchema);

export default userModel;